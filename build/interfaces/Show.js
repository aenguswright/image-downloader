"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _Show_id, _Show_competitions;
Object.defineProperty(exports, "__esModule", { value: true });
const Competition_1 = __importDefault(require("./Competition"));
class Show {
    constructor(arg) {
        var _a;
        _Show_id.set(this, void 0);
        _Show_competitions.set(this, void 0);
        __classPrivateFieldSet(this, _Show_id, (_a = arg.id) !== null && _a !== void 0 ? _a : 0, "f");
        __classPrivateFieldSet(this, _Show_competitions, this.createFromJson(arg), "f");
    }
    createFromJson(json) {
        const competitions = [];
        json.competitions.forEach((competition) => {
            competitions.push(new Competition_1.default(competition));
        });
        return competitions;
    }
    ;
    getId() { return __classPrivateFieldGet(this, _Show_id, "f"); }
    setId(id) { __classPrivateFieldSet(this, _Show_id, id, "f"); }
    getCompetitions() { return __classPrivateFieldGet(this, _Show_competitions, "f"); }
    setCompetitions(competitions) { __classPrivateFieldSet(this, _Show_competitions, competitions, "f"); }
}
exports.default = Show;
_Show_id = new WeakMap(), _Show_competitions = new WeakMap();
