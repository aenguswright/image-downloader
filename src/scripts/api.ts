import { GraphQLClient } from 'graphql-request';
import ServerParticulars from '../interfaces/ServerParticulars'
import Competitions from '../objects/Show'

export default class Api {
    token:string;
    graphQl:GraphQLClient
    show:Competitions
    constructor (arg:ServerParticulars) {
      this.token = arg.token;
      this.graphQl = new GraphQLClient(`${arg.serverAddress}api/graph/`, {
        headers: {
          Authorization: arg.token
        }
      });
      this.show = new Competitions(this.graphQl);
    }
  }
