"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ServerParticulars {
    constructor(username, token, serverAddress) {
        this.username = username;
        this.token = token;
        this.serverAddress = serverAddress;
    }
}
exports.default = ServerParticulars;
