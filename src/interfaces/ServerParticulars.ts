export default class ServerParticulars {
  username:string
  token:string
  serverAddress:string
  constructor (username:string, token:string, serverAddress:string) {
    this.username = username
    this.token = token;
    this.serverAddress = serverAddress;
  }
}