import Nation from './Nation';

export default class Athlete {
  #id:number
  #nation:Nation
  #firstName: string
  #familyName: string
  #imageUrl:string
  constructor (arg: importedAthlete) {
    this.#id = arg.id ?? 0;
    this.#nation = new Nation(arg.nation);
    this.#firstName = arg.firstName ?? '';
    this.#familyName = arg.familyName ?? '';
    this.#imageUrl = arg.imageUrl ?? '';
  }
  getId() { return this.#id }
  setId(id:number) { this.#id = id }

  getNation() { return this.#nation }
  setNation(nation:object|Nation) {
    if (nation.isPrototypeOf('Nation')) {
      this.#nation = nation as Nation
    } else {
      this.#nation = new Nation(nation as {ioc:string})
    }
  }

  getImageUrl() { return this.#imageUrl }
  setImageUrl(imageUrl:string) { this.#imageUrl = imageUrl }

  getFullName() {
    return this.#firstName + ' ' + this.#familyName;
  }
}

interface importedAthlete {
  id: number
  nation: any
  firstName: string
  familyName: string
  imageUrl: string
}