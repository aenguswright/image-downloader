import Competitor from './Competitor';

export default class Competition {
  #id:number
  #competitors:Array<Competitor>
  constructor (arg:importedCompetition) {
    this.#id = arg.id ?? 0;
    this.#competitors = this.competitorsFromObject(arg);
  }
  private competitorsFromObject(arg:importedCompetition):Array<Competitor> {
    const competitors = <Array<Competitor>>[]
    arg.competitors.forEach((competitor:any) => {
      competitors.push(new Competitor(competitor))
    })
    return competitors;
  }

  getId() { return this.#id }
  setId(id:number) { this.#id = id }

  getCompetitors() { return this.#competitors }
  setCompetitors(competitors: Array<Competitor>) { this.#competitors = competitors }
}

interface importedCompetition {
  id: number
  competitors: Array<Competitor>
}