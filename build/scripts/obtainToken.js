"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const ServerParticulars_1 = __importDefault(require("../interfaces/ServerParticulars"));
const readline_sync_1 = __importDefault(require("readline-sync"));
const chalk_1 = __importDefault(require("chalk"));
/**
 * This gets the token we need for the authorization
 */
function default_1() {
    return __awaiter(this, void 0, void 0, function* () {
        const serverAddress = 'https://spectatorjudginga14295f70.hana.ondemand.com/andromeda-1.0.0/';
        const username = yield getUserName(); //'aengus@blackhorse-one.com';
        const password = yield getPassword(); //'showtime';
        console.info(`Requesting token for ${username}...`);
        return new Promise(function (resolve, reject) {
            if (username.length === 0 || password.length === 0) {
                reject(console.log(chalk_1.default.red('Password or username is invalid')));
            }
            axios_1.default.post(`${serverAddress}login`, {
                email: username,
                password: password
            }).then(response => {
                let token = new ServerParticulars_1.default(username, response.headers.authorization, serverAddress);
                console.info('Obtained token!');
                resolve(token);
            }).catch(error => {
                reject(error.response);
            });
        }).catch(error => {
            var _a;
            if (error)
                console.error(`☹ failed to log in ${username} because of`, (_a = error.data) === null || _a === void 0 ? void 0 : _a.error);
        });
    });
}
exports.default = default_1;
/**
 * Gets the email response from the user.
 * @returns Promise(string)
 */
function getUserName() {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((res, rej) => {
            let response = readline_sync_1.default.question(chalk_1.default.yellow('Please enter your email: '));
            res(response);
        });
    });
}
/**
 * Gets the password response from the user.
 * @returns Promise(string)
 */
function getPassword() {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((res, rej) => {
            let password = readline_sync_1.default.question(chalk_1.default.yellow('Please enter your password: '), { hideEchoBack: true });
            res(password);
        });
    });
}
