import Api from './api';
import obtainToken from './obtainToken';
import ServerParticulars from '../interfaces/ServerParticulars';
import processResult from './processResult';
import Show  from '../interfaces/Show';
import readlineSync from 'readline-sync'
import chalk from 'chalk';

/**
 * Prompt the user for the show id we will be requesting, and then get the token for the transaction
 */
export default function ():void {
  obtainToken() // Get the token
    .then(serverInfo => {
      let server = serverInfo as ServerParticulars;
      let api = new Api(server);

      let id = Number(readlineSync.question(chalk.yellow('Please enter the show id: ')));
    
      console.info('Getting the show from server...');
      api.show.getCompetitions(id) // Get all athletes in show
        .then((result:any) => {
          if (result) {
            let show:Show = new Show(result);
            processResult(show as Show)
              .then(result => { console.log('...') });
          }
        })
    }).catch(err => {
      console.error('Closed because of err');
    })
}
