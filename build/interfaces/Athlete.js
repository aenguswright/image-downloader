"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _Athlete_id, _Athlete_nation, _Athlete_firstName, _Athlete_familyName, _Athlete_imageUrl;
Object.defineProperty(exports, "__esModule", { value: true });
const Nation_1 = __importDefault(require("./Nation"));
class Athlete {
    constructor(arg) {
        var _a, _b, _c, _d;
        _Athlete_id.set(this, void 0);
        _Athlete_nation.set(this, void 0);
        _Athlete_firstName.set(this, void 0);
        _Athlete_familyName.set(this, void 0);
        _Athlete_imageUrl.set(this, void 0);
        __classPrivateFieldSet(this, _Athlete_id, (_a = arg.id) !== null && _a !== void 0 ? _a : 0, "f");
        __classPrivateFieldSet(this, _Athlete_nation, new Nation_1.default(arg.nation), "f");
        __classPrivateFieldSet(this, _Athlete_firstName, (_b = arg.firstName) !== null && _b !== void 0 ? _b : '', "f");
        __classPrivateFieldSet(this, _Athlete_familyName, (_c = arg.familyName) !== null && _c !== void 0 ? _c : '', "f");
        __classPrivateFieldSet(this, _Athlete_imageUrl, (_d = arg.imageUrl) !== null && _d !== void 0 ? _d : '', "f");
    }
    getId() { return __classPrivateFieldGet(this, _Athlete_id, "f"); }
    setId(id) { __classPrivateFieldSet(this, _Athlete_id, id, "f"); }
    getNation() { return __classPrivateFieldGet(this, _Athlete_nation, "f"); }
    setNation(nation) {
        if (nation.isPrototypeOf('Nation')) {
            __classPrivateFieldSet(this, _Athlete_nation, nation, "f");
        }
        else {
            __classPrivateFieldSet(this, _Athlete_nation, new Nation_1.default(nation), "f");
        }
    }
    getImageUrl() { return __classPrivateFieldGet(this, _Athlete_imageUrl, "f"); }
    setImageUrl(imageUrl) { __classPrivateFieldSet(this, _Athlete_imageUrl, imageUrl, "f"); }
    getFullName() {
        return __classPrivateFieldGet(this, _Athlete_firstName, "f") + ' ' + __classPrivateFieldGet(this, _Athlete_familyName, "f");
    }
}
exports.default = Athlete;
_Athlete_id = new WeakMap(), _Athlete_nation = new WeakMap(), _Athlete_firstName = new WeakMap(), _Athlete_familyName = new WeakMap(), _Athlete_imageUrl = new WeakMap();
