"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _Competition_id, _Competition_competitors;
Object.defineProperty(exports, "__esModule", { value: true });
const Competitor_1 = __importDefault(require("./Competitor"));
class Competition {
    constructor(arg) {
        var _a;
        _Competition_id.set(this, void 0);
        _Competition_competitors.set(this, void 0);
        __classPrivateFieldSet(this, _Competition_id, (_a = arg.id) !== null && _a !== void 0 ? _a : 0, "f");
        __classPrivateFieldSet(this, _Competition_competitors, this.competitorsFromObject(arg), "f");
    }
    competitorsFromObject(arg) {
        const competitors = [];
        arg.competitors.forEach((competitor) => {
            competitors.push(new Competitor_1.default(competitor));
        });
        return competitors;
    }
    getId() { return __classPrivateFieldGet(this, _Competition_id, "f"); }
    setId(id) { __classPrivateFieldSet(this, _Competition_id, id, "f"); }
    getCompetitors() { return __classPrivateFieldGet(this, _Competition_competitors, "f"); }
    setCompetitors(competitors) { __classPrivateFieldSet(this, _Competition_competitors, competitors, "f"); }
}
exports.default = Competition;
_Competition_id = new WeakMap(), _Competition_competitors = new WeakMap();
