"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _Competitor_id, _Competitor_athlete;
Object.defineProperty(exports, "__esModule", { value: true });
const Athlete_1 = __importDefault(require("./Athlete"));
class Competitor {
    constructor(arg) {
        _Competitor_id.set(this, void 0);
        _Competitor_athlete.set(this, void 0);
        __classPrivateFieldSet(this, _Competitor_id, arg.id, "f");
        __classPrivateFieldSet(this, _Competitor_athlete, new Athlete_1.default(arg.athlete), "f");
    }
    getId() { return __classPrivateFieldGet(this, _Competitor_id, "f"); }
    ;
    setId(id) { __classPrivateFieldSet(this, _Competitor_id, id, "f"); }
    ;
    getAthlete() { return __classPrivateFieldGet(this, _Competitor_athlete, "f"); }
    ;
    setAthlete(athlete) {
        if (athlete.isPrototypeOf('Athlete')) {
            __classPrivateFieldSet(this, _Competitor_athlete, athlete, "f");
        }
        else {
            __classPrivateFieldSet(this, _Competitor_athlete, new Athlete_1.default(athlete), "f");
        }
    }
    ;
    getAthleteName() {
        return __classPrivateFieldGet(this, _Competitor_athlete, "f").getFullName();
    }
}
exports.default = Competitor;
_Competitor_id = new WeakMap(), _Competitor_athlete = new WeakMap();
