"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_request_1 = require("graphql-request");
const Show_1 = __importDefault(require("../objects/Show"));
class Api {
    constructor(arg) {
        this.token = arg.token;
        this.graphQl = new graphql_request_1.GraphQLClient(`${arg.serverAddress}api/graph/`, {
            headers: {
                Authorization: arg.token
            }
        });
        this.show = new Show_1.default(this.graphQl);
    }
}
exports.default = Api;
