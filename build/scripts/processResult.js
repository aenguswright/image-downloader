"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_fetch_1 = __importDefault(require("node-fetch"));
const chalk_1 = __importDefault(require("chalk"));
const rimraf_1 = __importDefault(require("rimraf"));
const fs_1 = __importDefault(require("fs"));
/**
 * We take the results of the request from the database and get unique lists of each athlete image,
 * and flag, and then we download and save each image
 * @param show type Show
 */
function default_1(show) {
    return __awaiter(this, void 0, void 0, function* () {
        // Get a unique list of Athletes which we will use to get each of the images for, as well
        // as a unique list of Nations
        let athletes = [];
        let nations = [];
        show.getCompetitions().forEach((competition) => {
            competition.getCompetitors().forEach((competitor) => {
                if (competitor.getAthlete().getImageUrl() &&
                    athletes.findIndex((athlete) => athlete.getId() === competitor.getAthlete().getId()) === -1) {
                    athletes.push(competitor.getAthlete());
                }
                if (nations.findIndex((nation) => nation.getIoc() === competitor.getAthlete().getNation().getIoc()) === -1) {
                    nations.push(competitor.getAthlete().getNation());
                }
            });
        });
        console.info('Downloading images...');
        // Now Get each one of them
        getFlagImages(nations);
        getAthleteImages(athletes);
        return;
    });
}
exports.default = default_1;
/**
 * We first remove the existing directory to delete any old version of the files, and then we create the new directory and save the files
 * @param athletes Array for each of the athletes
 */
function getAthleteImages(athletes) {
    rimraf_1.default('./output/portraits', () => {
        fs_1.default.mkdirSync('./output/portraits');
    });
    let promises = [];
    athletes.forEach((athlete) => {
        const requestUrl = athlete.getImageUrl();
        const stem = athlete.getImageUrl().substr(athlete.getImageUrl().split('/', 3).join('/').length + 1);
        promises.push(downloadImage(requestUrl, stem));
    });
    Promise.all(promises).then(() => { return console.info('Finished loading Portraits!\n'); });
}
/**
 * We first remove the existing directory to delete any old version of the files, and then we create the new directory and save the files
 * @param nations Array for each of the Nations
 */
function getFlagImages(nations) {
    rimraf_1.default('./output/flags', () => {
        fs_1.default.mkdirSync('./output/flags');
        fs_1.default.mkdirSync('./output/flags/svg');
    });
    let promises = [];
    nations.forEach((nation) => {
        const stem = `flags/svg/${nation.getIoc().toLowerCase()}.svg`;
        const requestUrl = `http://s3.eu-central-1.amazonaws.com/spectator-judging/${stem}.svg`;
        promises.push(downloadImage(requestUrl, stem));
    });
    Promise.all(promises).then(() => { return console.info('Finished loading flags!\n'); });
}
/**
 * The function will fetch the image, and then save it in the output folder.
 * @param requestUrl This is the URL which we send the image request to
 * @param stem The stem is the file name including the path inside of the output folder
 * @returns void
 */
function downloadImage(requestUrl, stem) {
    return new Promise((reject, resolve) => __awaiter(this, void 0, void 0, function* () {
        const response = yield node_fetch_1.default(requestUrl);
        const buffer = yield response.buffer();
        fs_1.default.writeFile(`./output/${stem}`, buffer, (err) => {
            if (err) {
                return reject(console.error(chalk_1.default.red(`Couldn't save`), stem));
            }
            else {
                return resolve(console.info(chalk_1.default.green(`Saved`), stem));
            }
        });
    })).then((buffer) => { }).catch((err) => { });
}
