import Athlete from './Athlete';

export default class Competitor {
  #id:number;
  #athlete:Athlete;
  constructor (arg: importedCompetitor) {
    this.#id = arg.id;
    this.#athlete = new Athlete(arg.athlete as any);
  }
  getId() { return this.#id };
  setId(id:number) { this.#id = id };

  getAthlete() { return this.#athlete };
  setAthlete(athlete:Athlete|object) { 
    if (athlete.isPrototypeOf('Athlete')) {
      this.#athlete = athlete as Athlete
    } else {
      this.#athlete = new Athlete(athlete as any)
    }
  };

  getAthleteName() {
    return this.#athlete.getFullName();
  }
}

interface importedCompetitor {
  id: number
  athlete: any
}