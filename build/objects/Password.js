"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const l = console.log;
const stdout = process.stdout;
const stdin = process.stdin;
const readline = require("readline");
class Password {
    constructor(opts = {
        ask: "password:",
        passLength: 99
    }) {
        this.passLength = 99;
        this.stdin = process.stdin;
        const { ask, passLength } = opts;
        this.ask = ask;
        this.passLength = passLength;
        this.input = '';
        this.stdin = stdin;
        this.self = this;
    }
    start() {
        stdout.write(this.ask);
        process.stdin.setRawMode(true);
        process.stdin.resume();
        process.stdin.setEncoding('utf-8');
        process.stdin.on("data", this.pn(this));
    }
    pn(me) {
        return (data) => {
            const c = data;
            const self = me;
            switch (c) {
                case '\u0004': // Ctrl-d
                case '\r':
                case '\n':
                    return self.enter();
                case '\u0003': // Ctrl-c
                    return self.ctrlc();
                default:
                    // backspace
                    if (c.charCodeAt(0) === 8)
                        return this.backspace();
                    else
                        return self.newchar(c);
            }
        };
    }
    enter() {
        stdin.removeListener('data', this.pn);
        l("\nYour password is: " + this.input);
        stdin.setRawMode(false);
        stdin.pause();
    }
    get() {
        process.stdin.removeListener('data', this.pn);
        process.stdin.setRawMode(false);
        process.stdin.pause();
        return this.input;
    }
    ctrlc() {
        stdin.removeListener('data', this.pn);
        stdin.setRawMode(false);
        stdin.pause();
    }
    newchar(c) {
        if (this.input.length != this.passLength) {
            this.input += c;
            stdout.write("*");
        }
    }
    backspace() {
        const pslen = this.ask.length;
        readline.cursorTo(stdout, (pslen + this.input.length) - 1, 0);
        stdout.write(" ");
        readline.moveCursor(stdout, -1, 0);
        this.input = this.input.slice(0, this.input.length - 1);
    }
}
exports.default = Password;
