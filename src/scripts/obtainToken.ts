import axios from 'axios';
import ServerParticulars from '../interfaces/ServerParticulars'
import readlineSync from 'readline-sync'
import chalk from 'chalk';

/** 
 * This gets the token we need for the authorization
 */ 
export default async function () {
  const serverAddress:string = 'https://spectatorjudginga14295f70.hana.ondemand.com/andromeda-1.0.0/';
  const username:string = await getUserName(); //'aengus@blackhorse-one.com';
  const password:string = await getPassword(); //'showtime';

  console.info(`Requesting token for ${username}...`)
  return new Promise(function (resolve, reject): void {
    
    if (username.length === 0 || password.length === 0) {
      reject(console.log(chalk.red('Password or username is invalid')));
    }
    
    axios.post(`${serverAddress}login`, {
      email: username,
      password: password
    }).then(response => {
      let token = new ServerParticulars(username, response.headers.authorization, serverAddress);
      console.info('Obtained token!')
      resolve(token);
    }).catch(error => {
      reject(error.response);
    })
  }).catch(error => {
    if (error) console.error(`☹ failed to log in ${username} because of`, error.data?.error);
  })
}

/**
 * Gets the email response from the user.
 * @returns Promise(string)
 */
async function getUserName ():Promise<string> {
  return new Promise((res, rej) => {
    let response = readlineSync.question(chalk.yellow('Please enter your email: '));
    res(response);
  }) 
}

/**
 * Gets the password response from the user.
 * @returns Promise(string)
 */
async function getPassword ():Promise<string> {
  return new Promise((res, rej) => {
    let password = readlineSync.question(chalk.yellow('Please enter your password: '), { hideEchoBack: true});
    res(password);
  })
}

