import Show from '../interfaces/Show';
import Nation from '../interfaces/Nation';
import Athlete from '../interfaces/Athlete';
import Competitor from '../interfaces/Competitor';
import Competition from '../interfaces/Competition';
import fetch from 'node-fetch';
import chalk from 'chalk';
import rimraf from 'rimraf';
import fs from 'fs';
import { resolve } from 'path/posix';
/**
 * We take the results of the request from the database and get unique lists of each athlete image,
 * and flag, and then we download and save each image
 * @param show type Show
 */
export default async function (show:Show) {
  // Get a unique list of Athletes which we will use to get each of the images for, as well
  // as a unique list of Nations
  let athletes:Array<Athlete> = [] as Array<Athlete>;
  let nations:Array<Nation> = [] as Array<Nation>;
  show.getCompetitions().forEach((competition:Competition) => {
    competition.getCompetitors().forEach((competitor:Competitor) => {
      if (competitor.getAthlete().getImageUrl() && 
        athletes.findIndex((athlete:Athlete) => athlete.getId() === competitor.getAthlete().getId()) === -1) {
        athletes.push(competitor.getAthlete())
      }
      if (nations.findIndex((nation:Nation) => nation.getIoc() === competitor.getAthlete().getNation().getIoc()) === -1) {
        nations.push(competitor.getAthlete().getNation())
      }
    })
  })

  console.info('Downloading images...')
  // Now Get each one of them
  getFlagImages(nations);
  getAthleteImages(athletes);
  return
}

/**
 * We first remove the existing directory to delete any old version of the files, and then we create the new directory and save the files
 * @param athletes Array for each of the athletes
 */
function getAthleteImages (athletes:Array<Athlete>) {
  rimraf('./output/portraits', () => {
    fs.mkdirSync('./output/portraits');
  })
  let promises: Array<Promise<void>> = []
  athletes.forEach((athlete:Athlete) => {
    const requestUrl:string = athlete.getImageUrl();
    const stem:string = athlete.getImageUrl().substr(athlete.getImageUrl().split('/', 3).join('/').length + 1);
    promises.push(downloadImage(requestUrl, stem))
  })
  Promise.all(promises).then(() => { return console.info('Finished loading Portraits!\n') });
}

/**
 * We first remove the existing directory to delete any old version of the files, and then we create the new directory and save the files
 * @param nations Array for each of the Nations
 */
function getFlagImages (nations:Array<Nation>) {
  rimraf('./output/flags', () => {
    fs.mkdirSync('./output/flags');
    fs.mkdirSync('./output/flags/svg');
  })
  let promises: Array<Promise<void>> = []
  nations.forEach((nation:Nation) => {
    const stem:string = `flags/svg/${nation.getIoc().toLowerCase()}.svg`;
    const requestUrl:string = `http://s3.eu-central-1.amazonaws.com/spectator-judging/${stem}.svg`;
    promises.push(downloadImage(requestUrl, stem))
  })
  Promise.all(promises).then(() => { return console.info('Finished loading flags!\n') });
}

/**
 * The function will fetch the image, and then save it in the output folder.
 * @param requestUrl This is the URL which we send the image request to
 * @param stem The stem is the file name including the path inside of the output folder
 * @returns void
 */
function downloadImage (requestUrl:string, stem:string):Promise<void> {
  return new Promise(async (reject, resolve) => {
    const response = await fetch(requestUrl);
    const buffer = await response.buffer();
    fs.writeFile(`./output/${stem}`, buffer as any, (err) =>  {
      if (err) {
        return reject(console.error(chalk.red(`Couldn't save`), stem));
      } else {
        return resolve(console.info(chalk.green(`Saved`), stem));
      }
    })
  }).then((buffer) => {}).catch((err) => { });
}