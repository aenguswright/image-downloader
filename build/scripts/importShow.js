"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const api_1 = __importDefault(require("./api"));
const obtainToken_1 = __importDefault(require("./obtainToken"));
const processResult_1 = __importDefault(require("./processResult"));
const Show_1 = __importDefault(require("../interfaces/Show"));
const readline_sync_1 = __importDefault(require("readline-sync"));
const chalk_1 = __importDefault(require("chalk"));
/**
 * Prompt the user for the show id we will be requesting, and then get the token for the transaction
 */
function default_1() {
    obtainToken_1.default() // Get the token
        .then(serverInfo => {
        let server = serverInfo;
        let api = new api_1.default(server);
        let id = Number(readline_sync_1.default.question(chalk_1.default.yellow('Please enter the show id: ')));
        console.info('Getting the show from server...');
        api.show.getCompetitions(id) // Get all athletes in show
            .then((result) => {
            if (result) {
                let show = new Show_1.default(result);
                processResult_1.default(show)
                    .then(result => { console.log('...'); });
            }
        });
    }).catch(err => {
        console.error('Closed because of err');
    });
}
exports.default = default_1;
