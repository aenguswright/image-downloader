"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var ServerParticulars_1 = __importDefault(require("./interfaces/ServerParticulars"));
var serverAddress = 'https://spectatorjudginga14295f70.hana.ondemand.com/andromeda-1.0.0/';
var username = 'aengus@blackhorse-one.com';
var password = 'showtime';
// this gets the token we need for the authorization
function default_1() {
    console.log("obtaining token for " + username);
    return new Promise(function (resolve, reject) {
        axios_1.default.post(serverAddress + "login", {
            email: username,
            password: password
        }).then(function (response) {
            var token = new ServerParticulars_1.default(username, response.headers.authorization, serverAddress);
            console.log(token);
            resolve(token);
        }).catch(function (error) {
            console.log("failed to log in " + username + " because of " + error);
            reject(error);
        });
    });
}
exports.default = default_1;
