import Competition from './Competition';
import Competitor from './Competitor';
import Athlete from './Athlete';
import Nation from './Nation';

export default class Show {
  #id:number
  #competitions:Array<Competition>
  constructor (arg:importedShow) {
    this.#id = arg.id ?? 0
    this.#competitions = this.createFromJson(arg)
  }

  private createFromJson(json:importedShow):Array<Competition> {
    const competitions = <Array<Competition>>[];
    json.competitions.forEach((competition:any) => {
      competitions.push(new Competition(competition))
    })
    return competitions;
  };

  getId() { return this.#id }
  setId(id:number) { this.#id = id }

  getCompetitions():Array<Competition> { return this.#competitions }
  setCompetitions(competitions: Array<Competition>) { this.#competitions = competitions }
}

interface importedShow {
  id:number
  competitions:Array<unknown>
}