export default class Nation {
  #ioc:string
  constructor (arg:{ioc: string}) {
    this.#ioc = arg.ioc
  }
  getIoc() { return this.#ioc }
  setIoc(ioc:string) {this.#ioc = ioc }
}