"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
require.extensions['.gql'] = function (module, filename) {
    module.exports = fs_1.default.readFileSync(filename, 'utf8');
};
class Competitions {
    constructor(graphQl) {
        this.graphQl = graphQl;
    }
    getCompetitions(id) {
        const variables = {
            id: id
        };
        const graphQl = this.graphQl;
        var filename = require.resolve('../graphql/getShow.gql');
        const query = String(fs_1.default.readFileSync(filename));
        return new Promise(function (resolve, reject) {
            graphQl.request(query, variables)
                .then(response => {
                resolve(response.show);
            });
        });
    }
}
exports.default = Competitions;
