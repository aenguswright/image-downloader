import fs from 'fs';
import { GraphQLClient } from 'graphql-request';
require.extensions['.gql'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};

export default class Competitions {
  graphQl:GraphQLClient
  constructor (graphQl:GraphQLClient) {
    this.graphQl = graphQl
  }

  getCompetitions (id:number) {
    const variables:object = {
      id: id
    };
    const graphQl = this.graphQl;
    var filename = require.resolve('../graphql/getShow.gql');
    const query = String(fs.readFileSync(filename));

    return new Promise(function (resolve, reject) {
      graphQl.request(query, variables)
        .then(response => {
          resolve(response.show)
        })
    })
  }
}
